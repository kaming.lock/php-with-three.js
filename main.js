import * as THREE from 'three';
import { GLTFLoader } from 'three/addons/loaders/GLTFLoader.js';

const scene = new THREE.Scene();
const camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);

const renderer = new THREE.WebGLRenderer();
renderer.setSize(window.innerWidth, window.innerHeight);
renderer.outputColorSpace = THREE.SRGBColorSpace;
document.body.appendChild(renderer.domElement);

const loader = new GLTFLoader();

var model;

loader.load('./assets/3d_model/goku.glb', function (gltf) {

  model = gltf.scene;


  model.traverse(function (child) {
    if (child.isMesh) {
      if (child.material.map) {
        let texture = new THREE.TextureLoader().load('./assets/texture/goku.png');
        texture.colorSpace = THREE.SRGBColorSpace;
        texture.flipY = false;
        texture.wrapS = THREE.RepeatWrapping;
        texture.wrapT = THREE.RepeatWrapping;

        child.texture = texture;
        child.material = new THREE.MeshBasicMaterial({ map: texture });
      }
    }
  });

  scene.add(model);

}, function (xhr) {

  console.log((xhr.loaded / xhr.total * 100) + '% loaded');

}, function (error) {

  console.error(error);

});

scene.background = new THREE.Color(1, 1, 1);
camera.position.y = 1;
camera.position.z = 5;

function animate() {
  requestAnimationFrame(animate);
  if (model) model.rotation.y += 0.01;
  renderer.render(scene, camera);
}
animate();